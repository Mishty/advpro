import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.*;
import javax.swing.JOptionPane; 

public class LoginForm extends JFrame implements ActionListener
{
   JButton button1, button2;
//DONT forget the constructor
 public LoginForm()
 {
   setTitle("Login Form");
   setLayout(new FlowLayout());
   setSize(600,400);
   setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
   
   //Components
   JLabel click1 = new JLabel("Click Me");
   add(click1);
   button1 = new JButton("Me");
   add(button1);
   button1.addActionListener(this);
   
   JLabel click2 = new JLabel("Click You");
   add(click2);
   button2 = new JButton("You");
   add(button2);
   button2.addActionListener(this);

  }

public void actionPerformed(ActionEvent ae)
{
   if(ae.getSource() == button1)
   {
      button1.setText("You've clicked Me ");
   }  
   
   if(ae.getSource() == button2)
   {
      button2.setText("You've clicked You ");
   }  
   
}

public static void main(String arg[])
{
   LoginForm form = new LoginForm();
   
   form.setVisible(true);
   
} 
}
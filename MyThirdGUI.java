import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.*;
import java.awt.Color;
import javax.swing.JOptionPane; 

public class MyThirdGUI extends JFrame implements ActionListener
{
   JButton button1, button2;
//DONT forget the constructor
 public MyThirdGUI()
 {
   setTitle("Login Form");
   setLayout(new FlowLayout());
   setSize(600,400);
   setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
   
   //Components
   JLabel click1 = new JLabel("Click Me");
   add(click1);
   button1 = new JButton("Me");
   add(button1);
   button1.addActionListener(this);
   
   JLabel click2 = new JLabel("Click You");
   add(click2);
   button2 = new JButton("You");
   add(button2);
   button2.addActionListener(this);

  }

public void actionPerformed(ActionEvent ae)
{
   if(ae.getSource() == button1)
   {
      getContentPane().setBackground(Color.BLUE);
   }  
   
   if(ae.getSource() == button2)
   {
      getContentPane().setBackground(Color.YELLOW);
   }  
   
}

public static void main(String arg[])
{
   MyThirdGUI form = new MyThirdGUI();
   
   form.setVisible(true);
   
} 
}
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.*;


public class LabelAndButton extends JFrame implements ActionListener
{
   JButton clicker;

   public LabelAndButton()
   {
      setTitle("LabelAndButton");
      //arrange components one after the other from left to right
      setLayout(new FlowLayout());
      setSize(400,200);
      
      JLabel label = new JLabel("Button Label");
      add(label);
      clicker = new JButton("Answer Here");
      clicker.addActionListener(this);
      add(clicker);
   }
   public void actionPerformed(ActionEvent ae)
   {
     clicker.setText("The button is clicked!");
   }
   public static void main(String args[])
   {
      LabelAndButton lab = new LabelAndButton();
      lab.setVisible(true);
   }
}
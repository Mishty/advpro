import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.*;
import javax.swing.JOptionPane; 

public class MyFirstGUI extends JFrame implements ActionListener
{
   //Constructor to build the Jframe in
   public MyFirstGUI()
   {
      setTitle("My first GUI");
      
      setLayout(new FlowLayout());
      
      //size of window frame in pixels
      setSize(400,400);
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      
      /*Components*/
      
      JButton jButton = new JButton("Click Me");
      
      //add to frame with add() method
      add(jButton);
      
      //add to actionlistener
      jButton.addActionListener(this);
   }
   
   public void actionPerformed(ActionEvent ae)
   {
   //dialog box could be messagedialog or showinputdialog
      JOptionPane.showMessageDialog(this, "You have clicked me");
   }
   
   //execute the frame
   public static void main(String arg[])
   {
   //initiate GUI object
      MyFirstGUI gui = new MyFirstGUI();
      gui.setVisible(true);

}
}